<head>
	<meta charset="UTF-8">
	<title>Test</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="stylesheet" type="text/css" href="{{ asset('ui/front-end') }}/css/animate.css">
	<!-- <link rel="stylesheet" type="text/css" href="{{ asset('ui/front-end') }}/css/bootstrap.min.css"> -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="{{ asset('ui/front-end') }}/css/line-awesome.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('ui/front-end') }}/css/line-awesome-font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('ui/front-end') }}/vendor/fontawesome-free/css/all.min.css" >
	<link rel="stylesheet" type="text/css" href="{{ asset('ui/front-end') }}/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('ui/front-end') }}/css/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('ui/front-end') }}/lib/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('ui/front-end') }}/lib/slick/slick-theme.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('ui/front-end') }}/css/style.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('ui/front-end') }}/css/responsive.css">
</head>

<img src="{{ asset('ui\front-end') }}\images/logo/talkies-logo-big.png" alt="">
<img src="{{ asset('ui/front-end') }}/images/logo/talkies-logo-big.png" alt="">
<h1>this is test area</h1>
<h3>------------------------</h3>
<div class="wrapper">
<header class="p-0">
			<div class="container">
				<div class="header-data d-flex align-items-center">
					<div class="logo">
						<a href="{{ asset('ui/front-end') }}/index.html" title="">
							<img src="{{ asset('ui/front-end') }}/images/logo/talkies-logo-big.png" height="" width="100px">
						</a>
					</div><!--logo end-->
					<div class="search-bar">
						<form>
							<input type="text" name="search" placeholder="Search...">
							<button type="submit"><i class="la la-search"></i></button>
						</form>
					</div><!--search-bar end-->
					<nav>
						<ul>
							<li>
								<a href="{{ asset('ui/front-end') }}/index.html" title="">
									<span><img src="{{ asset('ui/front-end') }}/images/icons/icon1.png" alt=""></span>
									Home
								</a>
							</li>
							<li>
								<a href="{{ asset('ui/front-end') }}/projects.html" title="">
									<span><img src="{{ asset('ui/front-end') }}/images/icons/icon3.png" alt=""></span>
									Projects
								</a>
							</li>
							<li>
								<a href="{{ asset('ui/front-end') }}/profiles.html" title="">
									<span><img src="{{ asset('ui/front-end') }}/images/icons/icon4.png" alt=""></span>
									Profiles
								</a>
								<ul>
									<li class="p-3 m-0"><a href="{{ asset('ui/front-end') }}/user-profile.html" title="">User Profile</a></li>
									<li class="p-3 m-0"><a href="{{ asset('ui/front-end') }}/my-profile-feed.html" title="">my-profile-feed</a></li>
								</ul>
							</li>
							<li>
								<a href="{{ asset('ui/front-end') }}/#" title="" class="not-box-openm">
									<span><img src="{{ asset('ui/front-end') }}/images/icons/icon6.png" alt=""></span>
									Messages
								</a>
								<div class="notification-box msg" id="message">
									<div class="nt-title">
										<h4>Setting</h4>
										<a href="{{ asset('ui/front-end') }}/#" title="">Clear all</a>
									</div>
									<div class="nott-list">
										<div class="notfication-details">
											<div class="noty-user-img">
												<img src="{{ asset('ui/front-end') }}/images/profile_pics/ny-img1.png" alt="">
											</div>
											<div class="notification-info">
												<h3><a href="{{ asset('ui/front-end') }}/messages.html" title="">Jassica William</a> </h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</p>
												<span>2 min ago</span>
											</div><!--notification-info -->
										</div>
										<div class="notfication-details">
											<div class="noty-user-img">
												<img src="{{ asset('ui/front-end') }}/images/profile_pics/ny-img2.png" alt="">
											</div>
											<div class="notification-info">
												<h3><a href="{{ asset('ui/front-end') }}/messages.html" title="">Jassica William</a></h3>
												<p>Lorem ipsum dolor sit amet.</p>
												<span>2 min ago</span>
											</div><!--notification-info -->
										</div>
										<div class="notfication-details">
											<div class="noty-user-img">
												<img src="{{ asset('ui/front-end') }}/images/profile_pics/ny-img3.png" alt="">
											</div>
											<div class="notification-info">
												<h3><a href="{{ asset('ui/front-end') }}/messages.html" title="">Jassica William</a></h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
													eiusmod tempo incididunt ut labore et dolore magna aliqua.</p>
												<span>2 min ago</span>
											</div><!--notification-info -->
										</div>
										<div class="view-all-nots">
											<a href="{{ asset('ui/front-end') }}/messages.html" title="">View All Messsages</a>
										</div>
									</div><!--nott-list end-->
								</div><!--notification-box end-->
							</li>
							<li>
								<a href="{{ asset('ui/front-end') }}/#" title="" class="not-box-open">
									<span><img src="{{ asset('ui/front-end') }}/images/icons/icon7.png" alt=""></span>
									Notification
								</a>
								<div class="notification-box noti" id="notification">
									<div class="nt-title">
										<h4>Setting</h4>
										<a href="{{ asset('ui/front-end') }}/#" title="">Clear all</a>
									</div>
									<div class="nott-list">
										<div class="notfication-details">
											<div class="noty-user-img">
												<img src="{{ asset('ui/front-end') }}/images/profile_pics/ny-img1.png" alt="">
											</div>
											<div class="notification-info">
												<h3><a href="{{ asset('ui/front-end') }}/#" title="">Jassica William</a> Comment on your project.
												</h3>
												<span>2 min ago</span>
											</div><!--notification-info -->
										</div>
										<div class="notfication-details">
											<div class="noty-user-img">
												<img src="{{ asset('ui/front-end') }}/images/profile_pics/ny-img2.png" alt="">
											</div>
											<div class="notification-info">
												<h3><a href="{{ asset('ui/front-end') }}/#" title="">Jassica William</a> Comment on your project.
												</h3>
												<span>2 min ago</span>
											</div><!--notification-info -->
										</div>
										<div class="notfication-details">
											<div class="noty-user-img">
												<img src="{{ asset('ui/front-end') }}/images/profile_pics/ny-img3.png" alt="">
											</div>
											<div class="notification-info">
												<h3><a href="{{ asset('ui/front-end') }}/#" title="">Jassica William</a> Comment on your project.
												</h3>
												<span>2 min ago</span>
											</div><!--notification-info -->
										</div>
										<div class="notfication-details">
											<div class="noty-user-img">
												<img src="{{ asset('ui/front-end') }}/images/profile_pics/ny-img2.png" alt="">
											</div>
											<div class="notification-info">
												<h3><a href="{{ asset('ui/front-end') }}/#" title="">Jassica William</a> Comment on your project.
												</h3>
												<span>2 min ago</span>
											</div><!--notification-info -->
										</div>
										<div class="view-all-nots">
											<a href="{{ asset('ui/front-end') }}/#" title="">View All Notification</a>
										</div>
									</div><!--nott-list end-->
								</div><!--notification-box end-->
							</li>
						</ul>
					</nav><!--nav end-->
					<div class="menu-btn">
						<a href="{{ asset('ui/front-end') }}/#" title=""><i class="fa fa-bars"></i></a>
					</div><!--menu-btn end-->
					<div class="user-account">
						<div class="user-info">
							<img src="{{ asset('ui/front-end') }}/images/profile_pics/user.png" alt="">
							<a href="{{ asset('ui/front-end') }}/#" title="">John</a>
							<i class="la la-sort-down"></i>
						</div>
						<div class="user-account-settingss" id="users">
							<h3>Online Status</h3>
							<ul class="on-off-status">
								<li>
									<div class="fgt-sec">
										<input type="radio" name="cc" id="c5">
										<label for="c5">
											<span></span>
										</label>
										<small>Online</small>
									</div>
								</li>
								<li>
									<div class="fgt-sec">
										<input type="radio" name="cc" id="c6">
										<label for="c6">
											<span></span>
										</label>
										<small>Offline</small>
									</div>
								</li>
							</ul>
							<h3>Custom Status</h3>
							<div class="search_form">
								<form>
									<input type="text" name="search">
									<button type="submit">Ok</button>
								</form>
							</div><!--search_form end-->
							<h3>Setting</h3>
							<ul class="us-links">
								<li><a href="{{ asset('ui/front-end') }}/profile-account-setting.html" title="">Account Setting</a></li>
								<li><a href="{{ asset('ui/front-end') }}/#" title="">Privacy</a></li>
								<li><a href="{{ asset('ui/front-end') }}/#" title="">Faqs</a></li>
								<li><a href="{{ asset('ui/front-end') }}/#" title="">Terms & Conditions</a></li>
							</ul>
							<h3 class="tc"><a href="{{ asset('ui/front-end') }}/sign-in.html" title="">Logout</a></h3>
						</div><!--user-account-settingss end-->
					</div>
				</div><!--header-data end-->
			</div>
		</header><!--header end-->
</div>